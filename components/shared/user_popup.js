import React, {Component} from 'react'
import Modal from 'react-responsive-modal'
import settings from '../../settings'
import {Link2} from 'react-feather'
export default class UserPopup extends Component{
    constructor(props){
        super(props);
        this.state = {
            isloading: false,
            user: {},
            open: this.props.open,
        }
        this.onOpenModal = this.onOpenModal.bind(this);
        this.onCloseModal = this.onCloseModal.bind(this);
    }
    onOpenModal() {
        this.setState({ open: true });
      };
    
      onCloseModal() {
        this.setState({ open: false }, ()=>{
            this.props.getInput(this.state.open);
        });
        
      };

    componentDidMount() {
        this.fetchUser(localStorage.token, this.props.user_id);
    }


    componentWillReceiveProps(nextProps) {
        if (this.props.user_id != nextProps.user_id) {
            //console.log(nextProps.scholarship_id)
            this.fetchUser(localStorage.token, nextProps.user_id);
        }
        if (!this.props.open && !!nextProps.open) {
            this.setState({open: nextProps.open});
        }
    }

    fetchUser(token, user_id) {
        //console.log(`${settings.urls.get_reward.replace('{reward_id}', reward_id )}`)
        this.setState({isloading: true});
        if (token && user_id) {
            fetch(settings.urls.admin_update_user.replace('{user_id}', user_id ), {
                method: 'GET',
                headers: {'Content-Type': 'application/json', 'Authorization': token},
                mode: 'cors',
            })
            .then(
                response => response.json()
            )
            .then(
                data => this.setState({isloading: false, user: data}, ()=>{
                    //console.log(data)
                })
            )
        }
    }
    
    render(){
        const {isAdmin, firstName, lastName, email, emailVerified, isDisabled, isActive, image, referralCode, referredBy, referralToken, firstLogin, coin, gpa, major, criteria, level, scholarshipCountry, applicantCountry, id} = this.state.user
        let {open} = this.state;
        return(
        <Modal className="more-modal" open={open} onClose={this.onCloseModal} showCloseIcon={true} little>
        {this.state.isloading ?
        <img className="popup-button-puff" src="/img/puff.svg"/>
        :   <React.Fragment>
            <div className="more-modal">
            <p><strong>ID:</strong> {id}</p>
            <p><strong>First Name:</strong> {firstName}</p>
            <p><strong>Last Name:</strong> {lastName}</p>
            <p><strong>Email:</strong> {email}</p>
            <p><strong>Email Verified:</strong> <span dangerouslySetInnerHTML={{ __html: `${emailVerified}` }}></span></p>
            <p><strong>Is Admin:</strong> <span dangerouslySetInnerHTML={{ __html: `${isAdmin}` }}></span></p>
            <p><strong>Image Link:</strong> {image}</p>
            <p><strong>Referral Code:</strong> {referralCode}</p>
            <p><strong>Referred By:</strong> {referredBy}</p>
            <p><strong>Referral Token:</strong> {referralToken}</p>
            <p><strong>First Login:</strong> <span dangerouslySetInnerHTML={{ __html: `${firstLogin}` }}></span></p>
            <p><strong>Coin:</strong> {coin}</p>
            <p><strong>GPA:</strong> {gpa}</p>
            <p><strong>Major:</strong> {major}</p>
            <p><strong>Criteria:</strong> {criteria}</p>
            <p><strong>Level:</strong> {level}</p>
            <p><strong>Scholarship Country:</strong> {scholarshipCountry}</p>
            <p><strong>Applicant Country:</strong> {applicantCountry}</p>
            </div>
            <br/>
            </React.Fragment>
        }
        </Modal>
        );
    }
}