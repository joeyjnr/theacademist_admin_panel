import React, {Component} from 'react'
import Modal from 'react-responsive-modal'
import settings from '../../settings'
import {Link2} from 'react-feather'
import Moment from 'react-moment';
export default class ApplicationPopup extends Component{
    constructor(props){
        super(props);
        this.state = {
            isloading: false,
            application: {},
            open: this.props.open,
        }
        this.onOpenModal = this.onOpenModal.bind(this);
        this.onCloseModal = this.onCloseModal.bind(this);
    }
    onOpenModal() {
        this.setState({ open: true });
      };
    
      onCloseModal() {
        this.setState({ open: false }, ()=>{
            this.props.getInput(this.state.open);
        });
        
      };

    componentDidMount() {
        this.fetchApplication(localStorage.token, this.props.application_id);
    }


    componentWillReceiveProps(nextProps) {
        if (this.props.application_id != nextProps.application_id) {
            //console.log(nextProps.scholarship_id)
            this.fetchApplication(localStorage.token, nextProps.application_id);
        }
        if (!this.props.open && !!nextProps.open) {
            this.setState({open: nextProps.open});
        }
    }

    fetchApplication(token, application_id) {
        this.setState({isloading: true});
        if (token && application_id) {
            fetch(settings.urls.get_application.replace('{application_id}', application_id ), {
                method: 'GET',
                headers: {'Content-Type': 'application/json', 'Authorization': token},
                mode: 'cors',
            })
            .then(
                response => response.json()
            )
            .then(
                data => this.setState({isloading: false, application: data})
            )
        }
    }
    
    render(){
        let 
        {
            applicationId, firstName, lastName, email, phone, description, dateOfBirth, paymentStatus, applicationStatus, gender, expectedSemesterEntry, level, firstChoiceMajor, secondChoiceMajor, satScore, actScore, toeflScore, gmatScore, greScore, ieltsScore, seekingScholarship, country, schoolCountry, gpa, comment, date, time, ip
        } = this.state.application;
        let {open} = this.state;
        return(
        <Modal className="more-modal" open={open} onClose={this.onCloseModal} showCloseIcon={true} little>
        {this.state.isloading ?
        <img className="popup-button-puff" src="/img/puff.svg"/>
        :   <React.Fragment>
            <div className="application-modal">
            <p><strong>Application ID:</strong> {applicationId}</p>
            <p><strong>First Name:</strong> {firstName}</p>
            <p><strong>Last Name:</strong> {lastName}</p>
            <p><strong>Email:</strong> {email}</p>
            <p><strong>Phone:</strong> {phone}</p>
            <p><strong>Description:</strong> {description}</p>
            <p><strong>Date of Birth:</strong> {dateOfBirth}</p>
            <p><strong>Payment Status:</strong> {paymentStatus}</p>
            <p><strong>Application Status:</strong> {applicationStatus}</p>
            <p><strong>Gender:</strong> {gender}</p>
            <p><strong>Expected Entry Semester:</strong> {expectedSemesterEntry}</p>
            <p><strong>Level:</strong> {level}</p>
            <p><strong>First Choice Major:</strong> {firstChoiceMajor}</p>
            <p><strong>Second Choice Major:</strong> {secondChoiceMajor}</p>
            <p><strong>SAT Score:</strong> {satScore}</p>
            <p><strong>ACT Score:</strong> {actScore}</p>
            <p><strong>TOEFL Score:</strong> {toeflScore}</p>
            <p><strong>GMAT Score:</strong> {gmatScore}</p>
            <p><strong>GRE Score:</strong> {greScore}</p>
            <p><strong>IELTS Score</strong> {ieltsScore}</p>
            <p><strong>Seeking Scholarship?</strong> {seekingScholarship}</p>
            <p><strong>Applicant Country</strong> {country}</p>
            <p><strong>School Country</strong> {schoolCountry}</p>
            <p><strong>GPA</strong> {gpa}</p>
            <p><strong>Appointment Date</strong>{date}</p>
            <p><strong>Appointment Time</strong> {time}</p>
            <p><strong>Comments</strong> {comment}</p>
            </div>
            <br/>
            </React.Fragment>
        }
        </Modal>
        );
    }
        
}