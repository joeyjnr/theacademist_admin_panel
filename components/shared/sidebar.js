import React, {Component} from 'react'
import {Link, withRouter, matchPath} from 'react-router-dom'
class NavItem extends Component {

    render () {
        const {to, location, children, exact} = this.props;
        const matchLocation = matchPath(location.pathname, {path: to, exact: !!exact});
        const li_class = (matchLocation)?'side current':'side';
        return (
            <li className={li_class}><Link to={to}>{children}</Link></li>
        );
    }
}

const NavItemWithLocation = withRouter(NavItem);

export default class Sidebar extends Component{
    render(){
        return(
      <div className="col-lg-3 col-md-3 sidebar">
      <nav>
                    <ul>
                        <NavItemWithLocation to="/" exact>
                            
                            <span className="nav-align">SCHOLARSHIPS</span>
                        </NavItemWithLocation>
                        <NavItemWithLocation to="/school/gpa">
                            
                            <span className="nav-align">School (GPA)</span>
                        </NavItemWithLocation>
                        <NavItemWithLocation to="/applications">
                            
                            <span className="nav-align">ADMISSIONS</span>
                        </NavItemWithLocation>
                        <NavItemWithLocation to="/blog">
                            
                            <span className="nav-align">BLOG</span>
                        </NavItemWithLocation>
                        <NavItemWithLocation to="/forum">
                        
                            <span className="nav-align">FORUM</span>
                        </NavItemWithLocation>
                        <NavItemWithLocation to="/comments">
                            
                            <span className="nav-align">BLOG COMMENTS</span>
                        </NavItemWithLocation>
                        <NavItemWithLocation to="/replies">
                            
                            <span className="nav-align">FORUM REPLIES</span>
                        </NavItemWithLocation>
                        <NavItemWithLocation to="/draw">
                            
                            <span className="nav-align">DRAWS</span>
                        </NavItemWithLocation>
                        <NavItemWithLocation to="/users">
                            
                            <span className="nav-align">USERS</span>
                        </NavItemWithLocation>
                        <NavItemWithLocation to="/transactions">
                            
                            <span className="nav-align">TRANSACTIONS</span>
                        </NavItemWithLocation>
                        <NavItemWithLocation to="/logout">
                            
                            <span className="nav-align">LOGOUT</span>
                        </NavItemWithLocation>

                    </ul>
                </nav>

      </div>
        );
    }
}