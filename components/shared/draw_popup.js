import React, {Component} from 'react'
import Modal from 'react-responsive-modal'
import settings from '../../settings'
import {Link2} from 'react-feather'
export default class DrawPopup extends Component{
    constructor(props){
        super(props);
        this.state = {
            isloading: false,
            reward: {},
            open: this.props.open,
        }
        this.onOpenModal = this.onOpenModal.bind(this);
        this.onCloseModal = this.onCloseModal.bind(this);
    }
    onOpenModal() {
        this.setState({ open: true });
      };
    
      onCloseModal() {
        this.setState({ open: false }, ()=>{
            this.props.getInput(this.state.open);
        });
        
      };

    componentDidMount() {
        this.fetchReward(localStorage.token, this.props.reward_id);
    }


    componentWillReceiveProps(nextProps) {
        if (this.props.reward_id != nextProps.reward_id) {
            //console.log(nextProps.scholarship_id)
            this.fetchReward(localStorage.token, nextProps.reward_id);
        }
        if (!this.props.open && !!nextProps.open) {
            this.setState({open: nextProps.open});
        }
    }

    fetchReward(token, reward_id) {
        //console.log(`${settings.urls.get_reward.replace('{reward_id}', reward_id )}`)
        this.setState({isloading: true});
        if (token && reward_id) {
            fetch(settings.urls.get_single_reward.replace('{reward_id}', reward_id ), {
                method: 'GET',
                headers: {'Content-Type': 'application/json', 'Authorization': token},
                mode: 'cors',
            })
            .then(
                response => response.json()
            )
            .then(
                data => this.setState({isloading: false, reward: data}, ()=>{
                    //console.log(data)
                })
            )
        }
    }
    
    render(){
        let 
        {firstName, lastName, email, drawType, user_id} = this.state.reward;
        let {open} = this.state;
        return(
        <Modal className="more-modal" open={open} onClose={this.onCloseModal} showCloseIcon={true} little>
        {this.state.isloading ?
        <img className="popup-button-puff" src="/img/puff.svg"/>
        :   <React.Fragment>
            <div className="more-modal">
            <p><strong>First Name:</strong> {firstName}</p>
            <p><strong>Last Name:</strong> {lastName}</p>
            <p><strong>Email:</strong> {email}</p>
            <p><strong>Draw Type:</strong> {drawType}</p>
            <p><strong>User ID:</strong> {user_id}</p>
            </div>
            <br/>
            </React.Fragment>
        }
        </Modal>
        );
    }
}