import React, {Component} from 'react'
import Footer from './shared/footer'
import Sidebar from './shared/sidebar'

export class App extends Component {
    render () {
        return (
            <div className="container-fluid">
            <div className="row">
            <Sidebar />
            <div className="col-md-10 main">
            {this.props.children}
            </div>
            </div>
                
            </div>
            
        );
    }
}