import React, {Component} from 'react'
import Navbar from '../../shared/navbar';
import Footer from '../../shared/footer'
import { Match, Link } from 'react-router-dom'
import {Trash2, Edit, Eye} from 'react-feather'
import Select from 'react-select';
import DatePicker from 'react-date-picker';
import Pagination from "react-js-pagination";
import settings from '../../../settings';
import moment from 'moment';
import Moment from 'react-moment';
import {toastr} from 'react-redux-toastr'
import _ from 'lodash';
import Modal from 'react-responsive-modal'
import ApplicationPopup from '../../shared/application_popup'
import {
    Tooltip,
  } from 'react-tippy';
  
export class ApplicationList extends Component{
    constructor(props){
        super(props)
        this.state = {
            scholarships: [],
            isloading: false,
            offset: 0,
            activePage: 1,
            resultCount: 0,
            opened: false,
            id: 0,
        }
        this.handlePageChange = this.handlePageChange.bind(this);
        this.OpenModal = this.OpenModal.bind(this);
        this.CloseModal = this.CloseModal.bind(this);
    }
    //to do verify props
    OpenModal(param) {
        this.setState({ opened: true, id: param }, ()=>{
            //this.getMore(param)
        });
        
      };
      CloseModal() {
        this.setState({ opened: false });
      };
    componentDidMount(){
        this.fetchApplications();
    }
    fetchApplications() {
        let {offset} = this.state
        this.setState({isloading: true});
            fetch(settings.urls.get_applications.replace('{off}', offset ), {
                method: 'GET',
                headers: {'Content-Type': 'application/json', 'Authorization': localStorage.token},
                mode: 'cors',
            })
            .then(
                response => response.json()
            )
            .then(
                data => this.setState({isloading: false, resultCount: data.count, scholarships: data.rows})
            )
        
    }
    handlePageChange(pageNumber) {
        console.log(`active page is ${pageNumber}`);
        //(n-1)15
        let mathStuff = pageNumber - 1;
        let multiplyStuff = mathStuff * 20;
        this.setState({activePage: pageNumber, offset: multiplyStuff }, ()=>{
            this.fetchScholarships()
            window.scrollTo(0, 0);
        });
         
    }
    deleteScholarship(id) {
        this.setState({isloading: true});
            fetch(settings.urls.delete_application.replace('{application_id}', id ), {
                method: 'DELETE',
                headers: {'Content-Type': 'application/json', 'Authorization': localStorage.token},
                mode: 'cors',
            })
            .then(
                response => response.json()
            )
            .then(
                data => this.setState({isloading: false}, ()=>{
                    toastr.success('Success!', 'Deleted Successfully');
                    this.fetchScholarships();
                })
            )
        
    }
    render(){
        let {scholarships, resultCount} = this.state;
        
        return <React.Fragment> 
<div className="row-fluid">
    <section className="help-center-section">
    
        <div className="row-fluid hero-box">
        <div className="col-md-12">
            <div className="headline-box">
            
            <h1 className="home-headline">All Applications</h1>
            
            </div>
        </div>
        </div>
    </section>
</div>

 <div className="row-fluid new-application-row">
 <div className="col-md-1">
 </div>
    <div className="col-md-10">
    <div className="col-spaced help-box scholarship-list">
   
    <div className="row article-sub-row">
    <div className="col-md-12">
    
    <br/>
    <span className="content">
    <table className="table scholarship-list">
                                <thead>
                                <tr>
                                    <th>Application ID</th>
                                    <th>First Name</th>
                                    <th>Appointment Date</th>
                                    <th>Payment Status</th>
                                    <th>Application Status</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                {scholarships.map((scholarship, id)=>
                                <tr key={id} >
                                    <td>{scholarship.applicationId}</td>
                                    <td>{scholarship.lastName}</td>
                                    <td><Moment format="MMM D, YYYY">
                                    {scholarship.date}
                                        </Moment></td>
                                    <td>{scholarship.paymentStatus}</td>
                                    <td>{scholarship.applicationStatus}</td>
                                    <td className="center-actions"><Eye className="clicker" onClick={this.OpenModal.bind(this, `${scholarship.id}`)}/>&nbsp; <Trash2 onClick={this.deleteScholarship.bind(this, `${scholarship.id}`)} className="delete-icon"/></td>
                                </tr>

                                )}
                                </tbody>
                     
                            </table>
    </span>
    </div>
    
    </div>
    {scholarships.length > 0 ?
            <div className="row pagination-row">
            <Pagination
            activePage={this.state.activePage}
            itemsCountPerPage={20}
            totalItemsCount={resultCount}
            pageRangeDisplayed={10}
            onChange={this.handlePageChange}
            />
            </div>
             : null }
    </div>
    </div>
    <div className="col-md-1">
      
    </div>
    <div className="clearfix">
    </div>
    <ApplicationPopup open={this.state.opened} application_id={this.state.id} getInput={this.CloseModal}/>
 </div>
<Footer />
</React.Fragment>
        
    }
}