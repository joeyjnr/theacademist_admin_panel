import React, {Component} from 'react'
import Navbar from '../../shared/navbar';
import Footer from '../../shared/footer'
import { Match} from 'react-router-dom'
import {User, Share} from 'react-feather'
import Select from 'react-select';
import DatePicker from 'react-date-picker';
import Switch from "react-switch";
import settings from '../../../settings';
import moment from 'moment';
import _ from 'lodash';
import {toastr} from 'react-redux-toastr'
import {
    Tooltip,
  } from 'react-tippy';
  
  const CRITERIAS = [
	{ label: 'Merit', value: 'Merit' },
    { label: 'Need', value: 'Need' }
];
const GPAS = [
    { label: '2.0', value: '2.0' },
    { label: '2.1', value: '2.1' },
    { label: '2.2', value: '2.2' },
    { label: '2.3', value: '2.3' },
    { label: '2.4', value: '2.4' },
    { label: '2.5', value: '2.5' },
    { label: '2.6', value: '2.6' },
    { label: '2.7', value: '2.7' },
    { label: '2.8', value: '2.8' },
    { label: '2.9', value: '2.9' },
    { label: '3.0', value: '3.0' },
    { label: '3.1', value: '3.1' },
    { label: '3.2', value: '3.2' },
    { label: '3.3', value: '3.3' },
    { label: '3.4', value: '3.4' },
    { label: '3.5', value: '3.5' },
    { label: '3.6', value: '3.6' },
    { label: '3.7', value: '3.7' },
    { label: '3.8', value: '3.8' },
    { label: '3.9', value: '3.9' },
    { label: '4.0', value: '4.0' }
];
const LEVELS = [
	{ label: 'Graduate', value: 'Graduate' },
    { label: 'Undergraduate', value: 'Undergraduate' }
];
const COUNTRIES = [
	{ label: 'US', value: 'US' },
	{ label: 'Canada', value: 'Canada' }
];
export class UserUpdate extends Component{
    constructor(props){
        super(props)
        
        this.state = {
            id: props.match.params.user_id,
            isAdmin: false,
            firstName: "",
            lastName: "",
            email: "",
            emailVerified: false,
            isDisabled: false,
            isActive: false,
            image: undefined,
            referralCode: "",
            referredBy: 0,
            referralToken: 0,
            firstLogin: false,
            coin: undefined,
            gpa: undefined,
            major: "",
            criteria: "",
            level: "",
            scholarshipCountry: "",
            applicantCountry: "",
            countries: [],
            majors: [],
            isloading: false
        }
        this.handleAdminChange = this.handleAdminChange.bind(this);
        this.handleCriteriaChange = this.handleCriteriaChange.bind(this);
        this.handleLevelChange = this.handleLevelChange.bind(this);
        this.handleCountryChange = this.handleCountryChange.bind(this);
        this.handleApplicantCountryChange = this.handleApplicantCountryChange.bind(this);
        this.handleGpaChange = this.handleGpaChange.bind(this);
        this.handleEmailVerifiedChange = this.handleEmailVerifiedChange.bind(this);
        this.handleFirstLoginChange = this.handleFirstLoginChange.bind(this);
    }
      
    handleAdminChange(isAdmin) {
        this.setState({ isAdmin });
      }
      handleEmailVerifiedChange(emailVerified){
        this.setState({ emailVerified });
      }
      handleFirstLoginChange(firstLogin){
        this.setState({ firstLogin });
      }
    componentWillMount(){
        let {id} = this.state
        this.getUser(id);
        this.fetchCountries();
        this.fetchMajors();
    }
    handleCountryChange (country) {
		//console.log('You\'ve selected:', country);
		this.setState({ country });
    }
    handleGpaChange (gpa) {
		//console.log('You\'ve selected:', gpa);
		this.setState({ gpa }, ()=>{
            //console.log(this.state.gpa)
        });
    }
    handleApplicantCountryChange(applicantCountry){
        this.setState({ applicantCountry });
    }
    handleCriteriaChange (criteria) {
		//console.log('You\'ve selected:', criteria);
		this.setState({ criteria }, ()=>{
            //console.log(this.state.criteria)
        });
    }
    handleLevelChange (level) {
        //console.log('You\'ve selected:', level);
        this.setState({ level });
    }
    handleMajorChange (major) {
		//console.log('You\'ve selected:', major);
		this.setState({ major }, ()=>{
            //console.log(this.state.major)
        });
    }
    handleCountryChange (scholarshipCountry) {
        //console.log('You\'ve selected:', scholarshipCountry);
        this.setState({ scholarshipCountry });
    }
    fetchMajors() {
        this.setState({isloading: true});
            fetch(settings.urls.get_majors, {
                method: 'GET',
                headers: {'Content-Type': 'application/json'},
                mode: 'cors',
            })
            .then(
                response => response.json()
            )
            .then(
                data => this.setState({isloading: false, majors: data})
            )
        
    }
    fetchCountries() {
        this.setState({isloading: true});
            fetch(settings.urls.get_countries, {
                method: 'GET',
                headers: {'Content-Type': 'application/json'},
                mode: 'cors',
            })
            .then(
                response => response.json()
            )
            .then(
                data => this.setState({isloading: false, countries: data})
            )
        
    }
    getUser(user_id){
        this.setState({isloading: true});
            fetch(settings.urls.admin_update_user.replace('{user_id}', user_id ), {
                method: 'GET',
                headers: {'Content-Type': 'application/json', 'Authorization': localStorage.token},
                mode: 'cors',
            })
            .then(
                response => response.json()
            )
            .then(
                data => {
                    if (data.error){
                        throw Error(data.error.message || 'Unknown fetch error');
                        toastr.error('Error!', 'An error occured, please try again')
                    }
                    this.setState({isloading: false, isAdmin: data.isAdmin,
                        firstName: data.firstName,
                        lastName: data.lastName,
                        email: data.email,
                        emailVerified: data.emailVerified,
                        isDisabled: data.isDisabled,
                        isActive: data.isActive,
                        image: data.image,
                        referralCode: data.referralCode,
                        referredBy: data.referredBy,
                        referralToken: data.referralToken,
                        firstLogin: data.firstLogin,
                        coin: data.coin,
                        gpa: data.gpa,
                        major: data.major,
                        criteria: data.criteria,
                        level: data.level,
                        scholarshipCountry: data.scholarshipCountry,
                        applicantCountry: data.applicantCountry}, ()=>{
                   // console.log(data);
                   //this.setState({loaded: true})
                  
                })
            }
            )
    }
    
    updateUser(){
        const {isAdmin, firstName, lastName, email, emailVerified, isDisabled, isActive, image, referralCode, referredBy, referralToken, firstLogin, coin, gpa, major, criteria, level, scholarshipCountry, applicantCountry, id} = this.state
        this.setState({isloading: true});
        //console.log(JSON.stringify({isAdmin, firstName, lastName, email, emailVerified, isDisabled, isActive, image, referralCode, referredBy, referralToken, firstLogin, coin, gpa, major, criteria, level, scholarshipCountry, applicantCountry}))
            fetch(settings.urls.admin_update_user.replace('{user_id}', id ), {
                method: 'PATCH',
                headers: {'Content-Type': 'application/json', 'Authorization': localStorage.token},
                mode: 'cors',
                body: JSON.stringify({isAdmin, firstName, lastName, email, emailVerified, isDisabled, isActive, image, referralCode, referredBy, referralToken, firstLogin, coin, gpa, major, criteria, level, scholarshipCountry, applicantCountry})
            })
            .then(
                response => response.json()
            )
            .then(
                data => {
                this.setState({isloading: false}, ()=>{
                    toastr.success('Success!', 'Updated Successfully')
                })
            }
            )     
            
    }
    render(){
        const {isAdmin, firstName, lastName, email, emailVerified, isDisabled, isActive, image, referralCode, referredBy, referralToken, firstLogin, coin, gpa, major, criteria, level, scholarshipCountry, applicantCountry} = this.state
        const options = this.state.majors;
        const gpas = GPAS;
        const levels = LEVELS;
        const criterias = CRITERIAS;
        const countries = COUNTRIES;
        const applicantCountries = this.state.countries;
        return <React.Fragment>
<div className="row-fluid">
    <section className="help-center-section">
        
        <div className="row-fluid hero-box">
        <div className="col-md-12">
            <div className="headline-box">
            
            <h1 className="home-headline">Update User</h1>
            
            </div>
        </div>
        </div>
    </section>
</div>

 <div className="row-fluid new-application-row">
 <div className="col-md-1">
 </div>
    <div className="col-md-10">
    <div className="col-spaced help-box">
   
    <div className="row article-sub-row">
    <div className="col-md-12">
    
    <br/>
    <span className="content">
    <div className="row">
    <div className="col-md-6">
    <Tooltip
    interactive
    position="top"
    arrow={true}
    hideOnClick={true}
    title="Required"
    trigger="click"
    >
    <span className="major-select"><input placeholder="First Name" className="textInput" type="text" value={firstName} onChange={e=>this.setState({firstName: e.target.value})}/></span>
    </Tooltip>
    </div>
    <div className="col-md-6">
    <Tooltip
    interactive
    position="top"
    arrow={true}
    hideOnClick={true}
    title="Required"
    trigger="click"
    >
    <span className="major-select"><input placeholder="Last Name" className="textInput" type="text"value={lastName} onChange={e=>this.setState({lastName: e.target.value})}/></span>
    </Tooltip>
    </div>
    </div>
    
    <div className="row">
    <div className="col-md-6">
    <Tooltip
    interactive
    position="top"
    arrow={true}
    hideOnClick={true}
    title="Required"
    trigger="click"
    >
    <span className="major-select"><input placeholder="Email" className="textInput" type="text"value={email} onChange={e=>this.setState({email: e.target.value})}/></span>
     </Tooltip>
    </div>
    <div className="col-md-6">
    <Tooltip
    interactive
    position="top"
    arrow={true}
    hideOnClick={true}
    title="Required"
    trigger="click"
    >
    <span className="major-select"><input placeholder="Referral Code" className="textInput" type="text"value={referralCode} onChange={e=>this.setState({referralCode: e.target.value})}/></span>
     </Tooltip>
    </div>
    </div>
    <div className="row">
    <div className="col-md-6">
    <Tooltip
    interactive
    position="top"
    arrow={true}
    hideOnClick={true}
    title="Required"
    trigger="click"
    > 
    <span className="major-select"><input placeholder="Coin" className="textInput" type="text"value={coin} onChange={e=>this.setState({coin: e.target.value})}/></span>
    </Tooltip>
    </div>
    <div className="col-md-6">
    <Tooltip
    interactive
    position="top"
    arrow={true}
    hideOnClick={true}
    title="Required"
    trigger="click"
    > 
    <label htmlFor="normal-switch">
        <span>Admin</span>
        <Switch
          onChange={this.handleAdminChange}
          checked={this.state.isAdmin}
          id="normal-switch"
        />
      </label>
    </Tooltip>
    </div>
    </div>
    <div className="row">
    <div className="col-md-6">
    <Tooltip
    interactive
    position="top"
    arrow={true}
    hideOnClick={true}
    title="Required"
    trigger="click"
    > 
    <span className="major-select"><input placeholder="Referred By" className="textInput" type="text"value={referredBy} onChange={e=>this.setState({referredBy: e.target.value})}/></span>
    </Tooltip>
    </div>
    <div className="col-md-6">
    <Tooltip
    interactive
    position="top"
    arrow={true}
    hideOnClick={true}
    title="Required"
    trigger="click"
    > 
    <span className="major-select"><input placeholder="Referred Token" className="textInput" type="text"value={referralToken} onChange={e=>this.setState({referralToken: e.target.value})}/></span>
    </Tooltip>
    </div>
    </div>
    <div className="row">
    <div className="col-md-6">
    <Tooltip
    interactive
    position="top"
    arrow={true}
    hideOnClick={true}
    title="Required"
    trigger="click"
    > 
    <label htmlFor="normal-switch">
        <span>First Login</span>
        <Switch
          onChange={this.handleFirstLoginChange}
          checked={this.state.firstLogin}
          id="normal-switch"
        />
      </label>
    </Tooltip>
    </div>
    <div className="col-md-6">
    <Tooltip
    interactive
    position="top"
    arrow={true}
    hideOnClick={true}
    title="Required"
    trigger="click"
    > 
    <label htmlFor="normal-switch">
        <span>Email Verified</span>
        <Switch
          onChange={this.handleEmailVerifiedChange}
          checked={this.state.emailVerified}
          id="normal-switch"
        />
      </label>
    </Tooltip>
    </div>
    </div>
    <div className="row">
    <div className="col-md-6">
    <Tooltip
    interactive
    position="top"
    arrow={true}
    hideOnClick={true}
    title="Gpa"
    trigger="click"
    > 
    <Select
                                            name="gpa"
                                            className="gpa-select"
                                            value={this.state.gpa}
                                            placeholder={gpa}
                                            multi={false}
                                            simpleValue
                                            onChange={this.handleGpaChange}
                                            options={gpas}
                                            searchable={false}
                                        />
    </Tooltip>
    </div>
    <div className="col-md-6">
    <Tooltip
    interactive
    position="top"
    arrow={true}
    hideOnClick={true}
    title="Major"
    trigger="click"
    > 
    <Select
                                            name="gpa"
                                            className="gpa-select"
                                            value={this.state.major}
                                            placeholder={major}
                                            multi={false}
                                            simpleValue
                                            onChange={this.handleMajorChange}
                                            options={options}
                                            searchable={true}
                                        />
    </Tooltip>
    </div>
    </div>
    <div className="row">
    <div className="col-md-6">
    <Tooltip
    interactive
    position="top"
    arrow={true}
    hideOnClick={true}
    title="Scholarship Country"
    trigger="click"
    >
    <Select
                                            name="form-field-name"
                                            className="major-select"
                                            value={this.state.scholarshipCountry}
                                            placeholder={scholarshipCountry}
                                            multi={false}
                                            simpleValue
                                            onChange={this.handleCountryChange}
                                            options={countries}
                                        />
    </Tooltip>
    </div>
    <div className="col-md-6">
    <Tooltip
    interactive
    position="top"
    arrow={true}
    hideOnClick={true}
    title="Applicant Country"
    trigger="click"
    >
    <Select
                                            name="applicantcountry"
                                            className="major-select"
                                            onBlurResetsInput={false}
                                            onSelectResetsInput={false}
                                            value={this.state.applicantCountry}
                                            placeholder={applicantCountry}
                                            simpleValue
                                            multi={false}
                                            clearable={true}
                                            onChange={this.handleApplicantCountryChange}
                                            options={applicantCountries}
                                            searchable={true}
                                        />
    </Tooltip>
    </div>
    </div>
    <div className="row">
    <div className="col-md-6">
    <Tooltip
    interactive
    position="top"
    arrow={true}
    hideOnClick={true}
    title="Level"
    trigger="click"
    >
    <Select
                                            name="form-field-name"
                                            className="major-select"
                                            value={this.state.level}
                                            placeholder={level}
                                            multi={false}
                                            simpleValue
                                            onChange={this.handleLevelChange}
                                            options={levels}
                                            searchable={false}
                                        />
    </Tooltip>
    </div>
    <div className="col-md-6">
    <Tooltip
    interactive
    position="top"
    arrow={true}
    hideOnClick={true}
    title="Criteria"
    trigger="click"
    >
    <Select
                                            name="form-field-name"
                                            className="major-select"
                                            onBlurResetsInput={false}
                                            onSelectResetsInput={false}
                                            value={this.state.criteria}
                                            placeholder={criteria}
                                            simpleValue
                                            multi={false}
                                            clearable={true}
                                            onChange={this.handleCriteriaChange}
                                            options={criterias}
                                            searchable={false}
                                        />
    </Tooltip>
    </div>
    </div>
    <br/>
    <span className="center-now"><button className="application-btn aligner" onClick={this.updateUser.bind(this)}><span className="user-info">Update User</span></button></span>
    
    <br/>
    
    </span>
    </div>
    
    </div>
    </div>
    </div>
    <div className="col-md-1">
      
    </div>
   
 </div>
<Footer />
</React.Fragment>
        
    }
}
