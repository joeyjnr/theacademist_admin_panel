import React, {Component} from 'react'
import Navbar from '../../shared/navbar';
import Footer from '../../shared/footer'
import { Match, Link } from 'react-router-dom'
import {Trash2, Edit, Plus, Upload, Eye} from 'react-feather'
import Select from 'react-select';
import DatePicker from 'react-date-picker';
import Pagination from "react-js-pagination";
import settings from '../../../settings';
import moment from 'moment';
import {toastr} from 'react-redux-toastr'
import _ from 'lodash';
import GpaPopup from '../../shared/gpa_popup'
import {
    Tooltip,
  } from 'react-tippy';
  
export class SchoolList extends Component{
    constructor(props){
        super(props)
        this.state = {
            schools: [],
            isloading: false,
            offset: 0,
            activePage: 1,
            resultCount: 0,
            id: 0
        }
        this.handlePageChange = this.handlePageChange.bind(this);
        this.OpenModal = this.OpenModal.bind(this);
        this.CloseModal = this.CloseModal.bind(this);
    }
    //to do verify props
    OpenModal(param) {
        this.setState({ opened: true, id: param }, ()=>{
            //this.getMore(param)
        });
        
      };
      CloseModal() {
        this.setState({ opened: false });
      };
    showFab(){
        let innerFabs = document.getElementsByClassName('inner-fabs')[0];
        innerFabs.classList.toggle('show');
    }
    doUpload(){
        this.props.history.push(location.state? location.state.from : {pathname: '/school/gpa/bulk-import'});
    }
    doCreate(){
        this.props.history.push(location.state? location.state.from : {pathname: '/school/gpa/create'});
    }
    componentDidMount(){
        this.fetchSchools();
    }
    fetchSchools() {
        let {offset} = this.state
        this.setState({isloading: true});
            fetch(settings.urls.get_schools.replace('{off}', offset ), {
                method: 'GET',
                headers: {'Content-Type': 'application/json', 'Authorization': localStorage.token},
                mode: 'cors',
            })
            .then(
                response => response.json()
            )
            .then(
                data => this.setState({isloading: false, resultCount: data.count, schools: data.rows})
            )
        
    }
    handlePageChange(pageNumber) {
        console.log(`active page is ${pageNumber}`);
        //(n-1)15
        let mathStuff = pageNumber - 1;
        let multiplyStuff = mathStuff * 20;
        this.setState({activePage: pageNumber, offset: multiplyStuff }, ()=>{
            this.fetchSchools()
            window.scrollTo(0, 0);
        });
         
    }
    deleteSchool(id) {
        this.setState({isloading: true});
            fetch(settings.urls.delete_school.replace('{school_id}', id ), {
                method: 'DELETE',
                headers: {'Content-Type': 'application/json', 'Authorization': localStorage.token},
                mode: 'cors',
            })
            .then(
                response => response.json()
            )
            .then(
                data => this.setState({isloading: false}, ()=>{
                    toastr.success('Success!', 'Deleted Successfully');
                    this.fetchSchools();
                })
            )
        
    }
    render(){
        let {schools, resultCount} = this.state;
        
        return <React.Fragment> 
<div className="row-fluid">
    <section className="help-center-section">
    
        <div className="row-fluid hero-box">
        <div className="col-md-12">
            <div className="headline-box">
            
            <h1 className="home-headline">All Schools Gpa</h1>
            
            </div>
        </div>
        </div>
    </section>
</div>

 <div className="row-fluid new-application-row">
 <div className="col-md-1">
 </div>
    <div className="col-md-10">
    <div className="col-spaced help-box scholarship-list">
   
    <div className="row article-sub-row">
    <div className="col-md-12">
    
    <br/>
    <span className="content">
    <table className="table scholarship-list">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>City</th>
                                    <th>State</th>
                                    <th>SAT</th>
                                    <th>ACT</th>
                                    <th>Level</th>
                                    <th>Gpa</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                {schools.map((scholarship, id)=>
                                <tr key={scholarship.id} >
                                    <td>{scholarship.name}</td>
                                    <td>{scholarship.city}</td>
                                    <td>{scholarship.state}</td>
                                    <td>{scholarship.sat}</td>
                                    <td>{scholarship.act}</td>
                                    <td>{scholarship.level}</td>
                                    <td>{scholarship.gpa}</td>
                                    <td className="center-actions"><Eye className="clicker" onClick={this.OpenModal.bind(this, `${scholarship.id}`)}/>&nbsp;<Link to={`/school/gpa/update/${scholarship.id}`}><Edit className="edit-icon"/></Link> &nbsp; <Trash2 onClick={this.deleteSchool.bind(this, `${scholarship.id}`)} className="delete-icon"/></td>
                                </tr>

                                )}
                                </tbody>
                     
                            </table>
    </span>
    </div>
    
    </div>
    {schools.length > 0 ?
            <div className="row pagination-row">
            <Pagination
            activePage={this.state.activePage}
            itemsCountPerPage={20}
            totalItemsCount={resultCount}
            pageRangeDisplayed={10}
            onChange={this.handlePageChange}
            />
            </div>
             : null }
    </div>
    </div>
    <div className="col-md-1">
      
    </div>
    <div className="clearfix">
    </div>
    <div className="inner-fabs">
  <div className="fab round" id="fab3" data-tooltip="Gpa Bulk Import" onClick={this.doUpload.bind(this)}><Upload /></div>
  <div className="fab round" id="fab2" data-tooltip="Create Gpa (School)" onClick={this.doCreate.bind(this)}><Plus /></div>
</div>
    <div className="fab round" id="fab1" onClick={this.showFab}><Plus /></div>
 </div>
 <GpaPopup open={this.state.opened} school_id={this.state.id} getInput={this.CloseModal}/>
<Footer />
</React.Fragment>
        
    }
}