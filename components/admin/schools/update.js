import React, {Component} from 'react'
import Navbar from '../../shared/navbar';
import Footer from '../../shared/footer'
import { Match, Link } from 'react-router-dom'
import {User, Share} from 'react-feather'
import Select from 'react-select';
import DatePicker from 'react-date-picker';
import settings from '../../../settings';
import moment from 'moment';
import _ from 'lodash';
import {toastr} from 'react-redux-toastr'
import {
    Tooltip,
  } from 'react-tippy';
  const LEVELS = [
    { label: 'All', value: 'All' },
	{ label: 'Graduate', value: 'Graduate' },
    { label: 'Undergraduate', value: 'Undergraduate' }
];
const SPANS = [
	{ label: '2', value: '2' },
    { label: '4', value: '4' }
];
const GPAS = [
    { label: '2.0', value: '2.0' },
    { label: '2.1', value: '2.1' },
    { label: '2.2', value: '2.2' },
    { label: '2.3', value: '2.3' },
    { label: '2.4', value: '2.4' },
    { label: '2.5', value: '2.5' },
    { label: '2.6', value: '2.6' },
    { label: '2.7', value: '2.7' },
    { label: '2.8', value: '2.8' },
    { label: '2.9', value: '2.9' },
    { label: '3.0', value: '3.0' },
    { label: '3.1', value: '3.1' },
    { label: '3.2', value: '3.2' },
    { label: '3.3', value: '3.3' },
    { label: '3.4', value: '3.4' },
    { label: '3.5', value: '3.5' },
    { label: '3.6', value: '3.6' },
    { label: '3.7', value: '3.7' },
    { label: '3.8', value: '3.8' },
    { label: '3.9', value: '3.9' },
    { label: '4.0', value: '4.0' }
];
export class SchoolUpdate extends Component{
    constructor(props){
        super(props)
        this.state = {
            name: '',
            description: '',
            level: '',
            span: '',
            gpa: 0,
            sat: null,
            act: null,
            zip: null,
            city: '',
            address: '',
            state: '',
            website: '',
            isloading: false,
            id: props.match.params.school_id,
        }
        this.handleLevelChange = this.handleLevelChange.bind(this);
        this.handleGpaChange = this.handleGpaChange.bind(this);
        this.handleSpanChange = this.handleSpanChange.bind(this);

    }

    componentWillMount(){
        let {id} = this.state
        this.getSchool(id)
    }
    getSchool(school_id){
        this.setState({isloading: true});
            fetch(settings.urls.get_school.replace('{school_id}', school_id ), {
                method: 'GET',
                headers: {'Content-Type': 'application/json', 'Authorization': localStorage.token},
                mode: 'cors',
            })
            .then(
                response => response.json()
            )
            .then(
                data => {
                    if (data.error){
                        throw Error(data.error.message || 'Unknown fetch error');
                        toastr.error('Error!', 'An error occured, please try again')
                    }
                    this.setState({isloading: false, name: data.name, description: data.description, level: data.level, gpa: data.gpa, span: data.span, website: data.website, sat: data.sat, act: data.act, zip: data.zip, city: data.city, address: data.address, state: data.state}, ()=>{
                   // console.log(data);
                   //this.setState({loaded: true})
                  
                })
            }
            )
    }
    handleLevelChange (level) {
		//console.log('You\'ve selected:', level);
		this.setState({ level });
    }

    handleSpanChange (span) {
		//console.log('You\'ve selected:', level);
		this.setState({ span });
    }

    handleGpaChange (gpa) {
		//console.log('You\'ve selected:', gpa);
		this.setState({ gpa }, ()=>{
            console.log(this.state.gpa)
        });
    }
    updateSchool(){
        const {name, description, level, gpa, span, website, sat, act, zip, city, address, state, id} = this.state
         this.setState({isloading: true});
        fetch(settings.urls.get_school.replace('{school_id}', id ), {
            method: 'PATCH',
            headers: {'Content-Type': 'application/json', 'Authorization': localStorage.token},
            mode: 'cors',
            body: JSON.stringify({name, description, level, gpa, span, website, sat, act, zip, city, address, state})
        })
        .then(
            response => response.json()
        )
        .then(
            data => {
            this.setState({isloading: false}, ()=>{
                toastr.success('Success!', 'Updated Successfully')
            })
        }
        )
    }
    
    render(){
        let {name, description, level, gpa, span, website, sat, act, zip, city, address, state} = this.state;

        const levels = LEVELS;
        const gpas = GPAS;
        const spans = SPANS;
        
        return <React.Fragment>
<div className="row-fluid">
    <section className="help-center-section">
        
        <div className="row-fluid hero-box">
        <div className="col-md-12">
            <div className="headline-box">
            
            <h1 className="home-headline">Create School (GPA)</h1>
            
            </div>
        </div>
        </div>
    </section>
</div>

 <div className="row-fluid new-application-row">
 <div className="col-md-1">
 </div>
    <div className="col-md-10">
    <div className="col-spaced help-box">
   
    <div className="row article-sub-row">
    <div className="col-md-12">
    
    <br/>
    <span className="content">
    <div className="row">
    <div className="col-md-6">
    <Tooltip
    interactive
    position="top"
    arrow={true}
    hideOnClick={true}
    title="Required"
    trigger="click"
    >
    <span className="major-select"><input placeholder="Name" className="textInput" type="text" value={name} onChange={e=>this.setState({name: e.target.value})}/></span>
    </Tooltip>
    </div>
    <div className="col-md-6">
    <Tooltip
    interactive
    position="top"
    arrow={true}
    hideOnClick={true}
    title="Required"
    trigger="click"
    >
    <span className="major-select"><input placeholder="Website" className="textInput" type="text"value={website} onChange={e=>this.setState({website: e.target.value})}/></span>
    </Tooltip>
    </div>
    </div>
    <div className="row">
    <div className="col-md-6">
    <Tooltip
    interactive
    position="left"
    arrow={true}
    hideOnClick={true}
    title="Criteria (required)"
    trigger="click"
    >
    <Select
        name="form-field-name"
        className="major-select"
        value={span}
        onBlurResetsInput={false}
		onSelectResetsInput={false}
        placeholder="Span"
        simpleValue
        multi={false}
        clearable={true}
        onChange={this.handleSpanChange}
        options={spans}
        searchable={false}
        />
    </Tooltip>
    </div>
    <div className="col-md-6">
    <Tooltip
    interactive
    position="top"
    arrow={true}
    hideOnClick={true}
    title="Required"
    trigger="click"
    >
    <span className="major-select"><input placeholder="SAT" className="textInput" type="text" value={sat} onChange={e=>this.setState({sat: e.target.value})}/></span>
    </Tooltip>
    </div>
    </div>
    <div className="row">
    <div className="col-md-6">
    <Tooltip
    interactive
    position="top"
    arrow={true}
    hideOnClick={true}
    title="Required"
    trigger="click"
    >
    <span className="major-select"><input placeholder="ACT" className="textInput" type="text" value={act} onChange={e=>this.setState({act: e.target.value})}/></span>
    </Tooltip>
    </div>
    <div className="col-md-6">
    <Tooltip
    interactive
    position="top"
    arrow={true}
    hideOnClick={true}
    title="Required"
    trigger="click"
    >
    <span className="major-select"><input placeholder="ZIP" className="textInput" type="text" value={zip} onChange={e=>this.setState({zip: e.target.value})}/></span>
    </Tooltip>
    </div>
    </div>
    <div className="row">
    <div className="col-md-6">
    <Tooltip
    interactive
    position="top"
    arrow={true}
    hideOnClick={true}
    title="Required"
    trigger="click"
    >
    <Select
        name="form-field-name"
        className="major-select"
        value={level}
        onBlurResetsInput={false}
		onSelectResetsInput={false}
        placeholder="Level"
        simpleValue
        multi={false}
        clearable={true}
        onChange={this.handleLevelChange}
        options={levels}
        searchable={false}
        />
    </Tooltip>
    </div>
    <div className="col-md-6">
    <Tooltip
    interactive
    position="top"
    arrow={true}
    hideOnClick={true}
    title="Required"
    trigger="click"
    >
    <span className="major-select"><input placeholder="City" className="textInput" type="text" value={city} onChange={e=>this.setState({city: e.target.value})}/></span>
    </Tooltip>
    </div>
    </div>
    <div className="row">
    <div className="col-md-6">
    <Tooltip
    interactive
    position="top"
    arrow={true}
    hideOnClick={true}
    title="Required"
    trigger="click"
    >
    <span className="major-select"><input placeholder="Address" className="textInput" type="text" value={address} onChange={e=>this.setState({address: e.target.value})}/></span>
    </Tooltip>
    </div>
    <div className="col-md-6">
    <Tooltip
    interactive
    position="top"
    arrow={true}
    hideOnClick={true}
    title="Required"
    trigger="click"
    >
    <span className="major-select"><input placeholder="State" className="textInput" type="text" value={state} onChange={e=>this.setState({state: e.target.value})}/></span>
    </Tooltip>
    </div>
    </div>
    <div className="row">
    <div className="col-md-6">
    <Tooltip
    interactive
    position="top"
    arrow={true}
    hideOnClick={true}
    title="Optional"
    trigger="click"
    >
        <Select
        name="form-field-name"
        className="major-select"
        value={gpa}
        onBlurResetsInput={false}
		onSelectResetsInput={false}
        placeholder="Gpa"
        simpleValue
        multi={false}
        clearable={true}
        onChange={this.handleGpaChange}
        options={gpas}
        searchable={false}
        />
    </Tooltip>
    </div>
    <div className="col-md-6">
    <Tooltip
    interactive
    position="top"
    arrow={true}
    hideOnClick={true}
    title="Required"
    trigger="click"
    >
       <span className="major-select"><input placeholder="description" className="textInput" type="text" value={description} onChange={e=>this.setState({description: e.target.value})}/></span>
    </Tooltip>
    </div>
    </div>
        
    <br/>
    <span className="center-now"><button className="application-btn aligner" onClick={this.updateSchool.bind(this)}><span className="user-info">Update School</span></button></span>
    
    <br/>
    
    </span>
    </div>
    
    </div>
    </div>
    </div>
    <div className="col-md-1">
      
    </div>
   
 </div>
<Footer />
</React.Fragment>
        
    }
}