import React, {Component} from 'react'
import Navbar from '../../shared/navbar';
import Footer from '../../shared/footer'
import { Match} from 'react-router-dom'
import {User, Share} from 'react-feather'
import jwtDecode from 'jwt-decode'
import Select from 'react-select';
import DatePicker from 'react-date-picker';
import settings from '../../../settings';
import moment from 'moment';
import _ from 'lodash';
import {toastr} from 'react-redux-toastr'
import CKEditor from "react-ckeditor-component";
import {
    Tooltip,
  } from 'react-tippy';
  

export class ForumCreate extends Component{
    constructor(props){
        super(props)
        
        this.state = {
            topic: '', 
            content: '', 
            isloading: false,
        }
        this.onChange = this. onChange.bind(this);
        this.updateContent = this.updateContent.bind(this);
    }
    updateContent(newContent) {
        this.setState({
            content: newContent
        })
    }
    onChange(evt){
        //console.log("onChange fired with event info: ", evt);
        var newContent = evt.editor.getData();
        this.setState({
          content: newContent
        })
      }
      
      
    componentWillMount(){
    }
    
    
    createForum(){
        const {topic, content} = this.state
        this.setState({isloading: true});
        
                            let user = jwtDecode(localStorage.token);
                            let by = user.id;
                           ////////////
            fetch(settings.urls.new_forum, {
                method: 'POST',
                headers: {'Content-Type': 'application/json', 'Authorization': localStorage.token},
                mode: 'cors',
                body: JSON.stringify({topic, content, by})
            })
            .then(
                response => response.json()
            )
            .then(
                data => {
                this.setState({isloading: false}, ()=>{
                    toastr.success('Success!', 'Created Successfully')
                })
            }
            )  
                       
         
    }
    render(){
        const {topic, content} = this.state
        
        return <React.Fragment>
<div className="row-fluid">
    <section className="help-center-section">
        
        <div className="row-fluid hero-box">
        <div className="col-md-12">
            <div className="headline-box">
            
            <h1 className="home-headline">Create Forum Post</h1>
            
            </div>
        </div>
        </div>
    </section>
</div>

 <div className="row-fluid new-application-row">
 <div className="col-md-1">
 </div>
    <div className="col-md-10">
    <div className="col-spaced help-box">
   
    <div className="row article-sub-row">
    <div className="col-md-12">
    
    <br/>
    <span className="content">
    <div className="row">
    <div className="col-md-12">
    <Tooltip
    interactive
    position="top"
    arrow={true}
    hideOnClick={true}
    title="Required"
    trigger="click"
    >
    <span className="major-select"><input placeholder="Topic" className="textInput" type="text" value={topic} onChange={e=>this.setState({topic: e.target.value})}/></span>
    </Tooltip>
    </div>
    </div>
    <div className="row">
    <div className="col-md-12">
    <Tooltip
    interactive
    position="top"
    arrow={true}
    hideOnClick={true}
    title="Required"
    trigger="click"
    >
   <CKEditor 
              activeClass="p10" 
              content={content} 
              events={{
                "change": this.onChange
              }}
             />
     </Tooltip>
    </div>
    </div>
        
    <br/>
    <span className="center-now"><button className="application-btn aligner" onClick={this.createForum.bind(this)}><span className="user-info">Create Forum Post</span></button></span>
    
    <br/>
    
    </span>
    </div>
    
    </div>
    </div>
    </div>
    <div className="col-md-1">
      
    </div>
   
 </div>
<Footer />
</React.Fragment>
        
    }
}