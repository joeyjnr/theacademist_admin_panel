import React, {Component} from 'react'
import Navbar from '../../shared/navbar';
import Footer from '../../shared/footer'
import { Match, Link } from 'react-router-dom'
import {Trash2, Edit} from 'react-feather'
import Select from 'react-select';
import DatePicker from 'react-date-picker';
import Pagination from "react-js-pagination";
import settings from '../../../settings';
import moment from 'moment';
import {toastr} from 'react-redux-toastr'
import _ from 'lodash';
import {
    Tooltip,
  } from 'react-tippy';
  
export class ReplyList extends Component{
    constructor(props){
        super(props)
        this.state = {
            posts: [],
            isloading: false,
            offset: 0,
            activePage: 1,
            resultCount: 0,
        }
        this.handlePageChange = this.handlePageChange.bind(this);

    }

    componentDidMount(){
        this.fetchPosts();
    }
    fetchPosts() {
        let {offset} = this.state
        this.setState({isloading: true});
            fetch(settings.urls.get_replies.replace('{off}', offset ), {
                method: 'GET',
                headers: {'Content-Type': 'application/json', 'Authorization': localStorage.token},
                mode: 'cors',
            })
            .then(
                response => response.json()
            )
            .then(
                data => this.setState({isloading: false, resultCount: data.count, posts: data.rows})
            )
        
    }
    handlePageChange(pageNumber) {
        console.log(`active page is ${pageNumber}`);
        //(n-1)15
        let mathStuff = pageNumber - 1;
        let multiplyStuff = mathStuff * 20;
        this.setState({activePage: pageNumber, offset: multiplyStuff }, ()=>{
            this.fetchPosts()
            window.scrollTo(0, 0);
        });
         
    }
    deletePost(id) {
        this.setState({isloading: true});
            fetch(settings.urls.delete_reply.replace('{reply_id}', id ), {
                method: 'DELETE',
                headers: {'Content-Type': 'application/json', 'Authorization': localStorage.token},
                mode: 'cors',
            })
            .then(
                response => response.json()
            )
            .then(
                data => this.setState({isloading: false}, ()=>{
                    toastr.success('Success!', 'Deleted Successfully');
                    this.fetchPosts();
                })
            )
        
    }
    render(){
        let {posts, resultCount} = this.state;
        
        return <React.Fragment> 
<div className="row-fluid">
    <section className="help-center-section">
    
        <div className="row-fluid hero-box">
        <div className="col-md-12">
            <div className="headline-box">
            
            <h1 className="home-headline">All Forum Replies</h1>
            
            </div>
        </div>
        </div>
    </section>
</div>

 <div className="row-fluid new-application-row">
 <div className="col-md-1">
 </div>
    <div className="col-md-10">
    <div className="col-spaced help-box scholarship-list">
   
    <div className="row article-sub-row">
    <div className="col-md-12">
    
    <br/>
    <span className="content">
    <table className="table scholarship-list">
                                <thead>
                                <tr>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Content</th>
                                    <th>Forum ID</th>
                                    <th>By</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                {posts.map((post, id)=>
                                <tr key={id} >
                                    <td>{post.firstName}</td>
                                    <td>{post.lastName}</td>
                                    <td>{post.content}</td>
                                    <td>{post.forumId}</td>
                                    <td>{post.by}</td>
                                    <td className="center-actions"><Trash2 onClick={this.deletePost.bind(this, `${post.id}`)} className="delete-icon"/></td>
                                </tr>

                                )}
                                </tbody>
                     
                            </table>
    </span>
    </div>
    
    </div>
    {posts.length > 0 ?
            <div className="row pagination-row">
            <Pagination
            activePage={this.state.activePage}
            itemsCountPerPage={20}
            totalItemsCount={resultCount}
            pageRangeDisplayed={10}
            onChange={this.handlePageChange}
            />
            </div>
             : null }
    </div>
    </div>
    <div className="col-md-1">
      
    </div>
    <div className="clearfix">
    </div>
 </div>
<Footer />
</React.Fragment>
        
    }
}