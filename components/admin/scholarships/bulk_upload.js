import React, {Component} from 'react'
import Navbar from '../../shared/navbar';
import Footer from '../../shared/footer'
import { Match} from 'react-router-dom'
import {User, Share} from 'react-feather'
import jwtDecode from 'jwt-decode'
import Select from 'react-select';
import DatePicker from 'react-date-picker';
import settings from '../../../settings';
import moment from 'moment';
import _ from 'lodash';
import {toastr} from 'react-redux-toastr'
import {
    Tooltip,
  } from 'react-tippy';
  

export class ScholarshipBulkImport extends Component{
    constructor(props){
        super(props)
        
        this.state = { 
            isloading: false,
            key: '',
            signedRequest: null,
            awsURL: '',
        }
    }
    componentWillMount(){
    }
    
    
    createBlog(){
        this.setState({isloading: true});
        if(this.fileUpload.files[0] != null) {
            // Upload image to S3
            const file = this.fileUpload.files[0];
            const fileKey = Date.now() + file.name;
            const contentType = file.type;
            const extension = file.type
        //const {key, signedRequest, featuredImage} = this.state;
        this.setState({isloading: true});
        //change to put
            fetch(settings.urls.upload, {
                method: 'POST',
                headers: {'Content-Type': 'application/json', 'Authorization': localStorage.token},
                mode: 'cors',
                body: JSON.stringify({contentType, extension, fileKey})
            })
            .then(
                response => response.json()
            )
            .then(
                data => this.setState({isloading: false, key: data.key, signedRequest: data.signedRequest}, ()=>{
                   
                    //console.log(data.url);
                    ///then
                    let thingy = this.fileUpload.files[0];
                    let uri = data.url;
                    const contentType = thingy.type
        const {signedRequest} = this.state;
        this.setState({isloading: true}, ()=>{
            //console.log("2nd");
        });
        //change to put
            fetch(signedRequest, {
                method: 'PUT',
                headers: {'Content-Type': contentType},
                body: thingy
            })   ////////////
            
                .then(response=>{
                    
                        
                            let url = uri;
                            let user = jwtDecode(localStorage.token);
                            let by = user.id;
                           ////////////
            fetch(settings.urls.scholarship_bulk_import, {
                method: 'POST',
                headers: {'Content-Type': 'application/json', 'Authorization': localStorage.token},
                mode: 'cors',
                body: JSON.stringify({url})
            })
            .then(
                response => response.json()
            )
            .then(
                data => {
                this.setState({isloading: false}, ()=>{
                    toastr.success('Success!', 'Imported Successfully')
                })
            }
            )  
                       
                })
                }) 
            )
        }
        else{
            toastr.error('Erro!', 'No File Selected')
        }

    }
    render(){
        
        return <React.Fragment>
<div className="row-fluid">
    <section className="help-center-section">
        
        <div className="row-fluid hero-box">
        <div className="col-md-12">
            <div className="headline-box">
            
            <h1 className="home-headline">Scholarship Bulk Import</h1>
            
            </div>
        </div>
        </div>
    </section>
</div>

 <div className="row-fluid new-application-row">
 <div className="col-md-1">
 </div>
    <div className="col-md-10">
    <div className="col-spaced help-box">
   
    <div className="row article-sub-row">
    <div className="col-md-12">
    
    <br/>
    <span className="content">
    
    <div className="row">
    <div className="col-md-12">
    <span className="major-select"><input type="file" className="image-upload" accept=".csv" ref={ref => this.fileUpload = ref}/></span>
    </div>
    </div>
    <br/>
    <span className="center-now"><button className="application-btn aligner" onClick={this.createBlog.bind(this)}><span className="user-info">Import</span></button></span>
    
    <br/>
    
    </span>
    </div>
    
    </div>
    </div>
    </div>
    <div className="col-md-1">
      
    </div>
   
 </div>
<Footer />
</React.Fragment>
        
    }
}