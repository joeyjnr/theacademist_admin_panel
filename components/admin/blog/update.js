import React, {Component} from 'react'
import Navbar from '../../shared/navbar';
import Footer from '../../shared/footer'
import { Match} from 'react-router-dom'
import {User, Share} from 'react-feather'
import Select from 'react-select';
import DatePicker from 'react-date-picker';
import settings from '../../../settings';
import moment from 'moment';
import _ from 'lodash';
import {toastr} from 'react-redux-toastr'
import CKEditor from "react-ckeditor-component";
import {
    Tooltip,
  } from 'react-tippy';
  

export class BlogUpdate extends Component{
    constructor(props){
        super(props)
        
        this.state = {
            topic: '', 
            content: '', 
            urlParam: '', 
            featuredImage: '', 
            by: 0,
            id: props.match.params.blog_id,
            key: '',
            signedRequest: null,
            awsURL: '',
            loaded: false,
            test: "<p>Hey</p>"
        }
        this.onChange = this. onChange.bind(this);
        this.updateContent = this.updateContent.bind(this);
    }
    updateContent(newContent) {
        this.setState({
            content: newContent
        })
    }
    onChange(evt){
        //console.log("onChange fired with event info: ", evt);
        var newContent = evt.editor.getData();
        this.setState({
          content: newContent
        })
      }
      
      
    componentWillMount(){
        let {id} = this.state
        this.getBlog(id)
    }
    
    getBlog(blog_id){
        this.setState({isloading: true});
            fetch(settings.urls.get_singleblog.replace('{blog_id}', blog_id ), {
                method: 'GET',
                headers: {'Content-Type': 'application/json', 'Authorization': localStorage.token},
                mode: 'cors',
            })
            .then(
                response => response.json()
            )
            .then(
                data => {
                    if (data.error){
                        throw Error(data.error.message || 'Unknown fetch error');
                        toastr.error('Error!', 'An error occured, please try again')
                    }
                    this.setState({isloading: false, topic: data.topic, content: data.content, urlParam: data.urlParam, featuredImage: data.featuredImage, by: data.by, loaded: true}, ()=>{
                   // console.log(data);
                   //this.setState({loaded: true})
                  
                })
            }
            )
    }
    
    updateBlog(){
        const {topic, content, urlParam, featuredImage, by, id} = this.state
        this.setState({isloading: true});
        if(this.fileUpload.files[0] != null) {
            // Upload image to S3
            const file = this.fileUpload.files[0];
            const fileKey = Date.now() + file.name;
            const contentType = file.type;
            const extension = file.type
        //const {key, signedRequest, featuredImage} = this.state;
        this.setState({isloading: true});
        //change to put
            fetch(settings.urls.upload, {
                method: 'POST',
                headers: {'Content-Type': 'application/json', 'Authorization': localStorage.token},
                mode: 'cors',
                body: JSON.stringify({contentType, extension, fileKey})
            })
            .then(
                response => response.json()
            )
            .then(
                data => this.setState({isloading: false, key: data.key, signedRequest: data.signedRequest}, ()=>{
                   
                    //console.log(data.url);
                    ///then
                    let thingy = this.fileUpload.files[0];
                    let uri = data.url;
                    const contentType = thingy.type
        const {signedRequest} = this.state;
        this.setState({isloading: true}, ()=>{
            console.log("2nd");
        });
        //change to put
            fetch(signedRequest, {
                method: 'PUT',
                headers: {'Content-Type': contentType},
                body: thingy
            })   ////////////
            
                .then(response=>{
                    
                        
                            let featuredImage = uri;
                           ////////////
            fetch(settings.urls.get_singleblog.replace('{blog_id}', id ), {
                method: 'PATCH',
                headers: {'Content-Type': 'application/json', 'Authorization': localStorage.token},
                mode: 'cors',
                body: JSON.stringify({topic, content, urlParam, featuredImage, by})
            })
            .then(
                response => response.json()
            )
            .then(
                data => {
                this.setState({isloading: false}, ()=>{
                    toastr.success('Success!', 'Updated Successfully')
                })
            }
            )  
                       
                })
                }) 
            )
        }
        else{
            const {topic, content, urlParam, featuredImage, by, id} = this.state
            fetch(settings.urls.get_singleblog.replace('{blog_id}', id ), {
                method: 'PATCH',
                headers: {'Content-Type': 'application/json', 'Authorization': localStorage.token},
                mode: 'cors',
                body: JSON.stringify({topic, content, urlParam, featuredImage, by})
            })
            .then(
                response => response.json()
            )
            .then(
                data => {
                this.setState({isloading: false}, ()=>{
                    toastr.success('Success!', 'Updated Successfully')
                })
            }
            )     
            
        }
     

    }
    render(){
        const {topic, content, urlParam, featuredImage, by} = this.state
        
        return <React.Fragment>
<div className="row-fluid">
    <section className="help-center-section">
        
        <div className="row-fluid hero-box">
        <div className="col-md-12">
            <div className="headline-box">
            
            <h1 className="home-headline">Update Blog Post</h1>
            
            </div>
        </div>
        </div>
    </section>
</div>

 <div className="row-fluid new-application-row">
 <div className="col-md-1">
 </div>
    <div className="col-md-10">
    <div className="col-spaced help-box">
   
    <div className="row article-sub-row">
    <div className="col-md-12">
    
    <br/>
    <span className="content">
    <div className="row">
    <div className="col-md-12">
    <Tooltip
    interactive
    position="top"
    arrow={true}
    hideOnClick={true}
    title="Required"
    trigger="click"
    >
    <span className="major-select"><input placeholder="Topic" className="textInput" type="text" value={topic} onChange={e=>this.setState({topic: e.target.value})}/></span>
    </Tooltip>
    </div>
    </div>
    <div className="row">
    <div className="col-md-12">
    <Tooltip
    interactive
    position="top"
    arrow={true}
    hideOnClick={true}
    title="Required"
    trigger="click"
    >
    <span className="major-select"><input placeholder="URL Slug" className="textInput" type="text"value={urlParam} onChange={e=>this.setState({urlParam: e.target.value})}/></span>
    </Tooltip>
    </div>
    </div>
    <div className="row">
    <div className="col-md-12">
    <span className="major-select"><input type="file" className="image-upload" accept="image/*" ref={ref => this.fileUpload = ref}/></span>
    </div>
    </div>
    <div className="row">
    <div className="col-md-12">
    <Tooltip
    interactive
    position="top"
    arrow={true}
    hideOnClick={true}
    title="Required"
    trigger="click"
    >
    {this.state.loaded ?
   <CKEditor 
              activeClass="p10" 
              content={content} 
              events={{
                "change": this.onChange
              }}
             />
    :
    null
            }
     </Tooltip>
    </div>
    </div>
        
    <br/>
    <span className="center-now"><button className="application-btn aligner" onClick={this.updateBlog.bind(this)}><span className="user-info">Update Blog</span></button></span>
    
    <br/>
    
    </span>
    </div>
    
    </div>
    </div>
    </div>
    <div className="col-md-1">
      
    </div>
   
 </div>
<Footer />
</React.Fragment>
        
    }
}
