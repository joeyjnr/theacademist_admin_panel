import React, {Component} from 'react'
import { connect } from 'react-redux';
import {BrowserRouter, Route, Switch, Redirect} from 'react-router-dom';


import {App} from './components/app'
import {Home} from './components/home'
import Login from './components/accounts/login'
import {Forgot} from './components/accounts/forgot'
import {Logout} from './components/accounts/logout'
import {SchoolList} from './components/admin/schools/index'
import {SchoolCreate} from './components/admin/schools/create'
import {SchoolUpdate} from './components/admin/schools/update'
import {ScholarshipCreate} from './components/admin/scholarships/create'
import {ScholarshipList} from './components/admin/scholarships/index'
import {ScholarshipUpdate} from './components/admin/scholarships/update'
import {ScholarshipBulkImport} from './components/admin/scholarships/bulk_upload'
import {SchoolBulkImport} from './components/admin/schools/bulk_upload'
import {BlogList} from './components/admin/blog/index'
import {BlogUpdate} from './components/admin/blog/update'
import {BlogCreate} from './components/admin/blog/create'
import {ForumList} from './components/admin/forum/index'
import {ForumUpdate} from './components/admin/forum/update'
import {ForumCreate} from './components/admin/forum/create'
import {CommentList} from './components/admin/blog_comments/index'
import {ReplyList} from './components/admin/forum_replies/index'
import {UserList} from './components/admin/users/index'
import {UserUpdate} from './components/admin/users/update'
import {TransactionList} from './components/admin/transactions/index'
import {DrawList} from './components/admin/draw/index'
import {ApplicationList} from './components/admin/applications/index'
const EmptyComponent = () => <div>Empty yet</div>;


const AdminArea = connect(
    state=>({admin: localStorage.isAdmin, token: state.user.token})
)(
    props=>props.admin && props.token ?
        props.children
        :
        <Redirect to={{
            pathname: '/login',
            state: { from: props.location }
        }}/>
);

export const Router = props => (
    <BrowserRouter basename="/admin">
        <Switch>
        <Route path="/login" component={Login}/>
        <Route path="/logout" component={Logout}/>
        <AdminArea>
            <App>
            <Route exact path="/" component={ScholarshipList} />
            <Route exact path="/scholarship/create" component={ScholarshipCreate} />
            <Route exact path="/scholarship/update/:scholarship_id" component={ScholarshipUpdate} />
            <Route exact path="/scholarship/bulk-import" component={ScholarshipBulkImport} />
            <Route exact path="/school/gpa/create" component={SchoolCreate} />
            <Route exact path="/school/gpa" component={SchoolList} />
            <Route exact path="/school/gpa/update/:school_id" component={SchoolUpdate} />
            <Route exact path="/school/gpa/bulk-import" component={SchoolBulkImport} />
            <Route exact path="/blog" component={BlogList} />
            <Route exact path="/blog/update/:blog_id" component={BlogUpdate} />
            <Route exact path="/blog/create" component={BlogCreate} />
            <Route exact path="/comments" component={CommentList} />
            <Route exact path="/forum" component={ForumList} />
            <Route exact path="/forum/update/:forum_id" component={ForumUpdate} />
            <Route exact path="/forum/create" component={ForumCreate} />
            <Route exact path="/replies" component={ReplyList} />
            <Route exact path="/users" component={UserList} />
            <Route exact path="/users/update/:user_id" component={UserUpdate} />
            <Route exact path="/transactions" component={TransactionList} />
            <Route exact path="/draw" component={DrawList} />
            <Route exact path="/applications" component={ApplicationList} />
            </App>
        </AdminArea>
        </Switch>
    </BrowserRouter>
);
